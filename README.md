# OpenML dataset: house_16H

https://www.openml.org/d/574

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This database was designed on the basis of data provided by US Census
Bureau [http://www.census.gov] (under Lookup Access
[http://www.census.gov/cdrom/lookup]: Summary Tape File 1). The data
were collected as part of the 1990 US census. These are mostly counts
cumulated at different survey levels. For the purpose of this data set
a level State-Place was used. Data from all states was obtained. Most
of the counts were changed into appropriate proportions.  There are 4
different data sets obtained from this database: House(8H) House(8L)
House(16H) House(16L) These are all concerned with predicting the
median price of the house in the region based on demographic
composition and a state of housing market in the region. A number in
the name signifies the number of attributes of the data set. A
following letter denotes a very rough approximation to the difficulty
of the task. For Low task difficulty, more correlated attributes were
chosen as signified by univariate smooth fit of that input on the
target. Tasks with High difficulty have had their attributes chosen to
make the modelling more difficult due to higher variance or lower
correlation of the inputs to the target.

Original source: DELVE repository of data.
Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
Characteristics: 22784 cases, 17 continuous attributes.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/574) of an [OpenML dataset](https://www.openml.org/d/574). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/574/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/574/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/574/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

